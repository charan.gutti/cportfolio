import emailjs from "@emailjs/browser";
import gsap from "gsap";
import React, { useEffect, useRef, useState } from "react";
import { RxCross1 } from "react-icons/rx";

const Info = () => {
  const rightSlide = useRef();
  const leftSlide = useRef();
  const form = useRef();
  const [sentAlert, setSentAlert] = useState(false);

  const animations = () => {
    const ani = gsap.context(() => {
      gsap.fromTo(
        rightSlide.current,
        { duration: 1, x: 200, opacity: 0, delay: 0.5 },
        {
          duration: 1,
          x: 0,
          opacity: 1,
          delay: 0.5,
          ease: "power2",
          scrollTrigger: {
            trigger: rightSlide.current,
            start: "top 70%",
            end: "top 10%",
            scrub: true,
            markers: false,
          },
        }
      );
      gsap.fromTo(
        leftSlide.current,
        { duration: 1, x: -200, opacity: 0, delay: 0.5 },
        {
          duration: 1,
          x: 0,
          opacity: 1,
          delay: 0.5,
          ease: "power2",
          scrollTrigger: {
            trigger: leftSlide.current,
            start: "top 70%",
            end: "top 10%",
            scrub: true,
            markers: false,
          },
        }
      );
    });
    return () => ani.revert();
  };
  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_7xfxc4q",
        "template_38jm1gn",
        form.current,
        "fUnQR7juNe3Xi7ivx"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    setSentAlert(true);
  };

  useEffect(() => {
    animations();
  });
  return (
    <div className="w-full text-black bg-transparent " id="info">
      <div className="flex flex-col items-center gap-3 p-3 sm:justify-between sm:flex-row columns-2 sm:p-6">
        <div
          className="flex flex-col w-3/4 sm:w-1/2 sm:self-start"
          ref={leftSlide}
        >
          <h1 className="text-3xl text-center">About me</h1>
          <div className="p-6">
            <p className="text-center sm:text-left sm:text-lg">
              I am a college student studying at ICFAI Tech, Hyderabad. I am a
              person who is very practical and has a great record of submitting
              work on time with the best quality possible. I have been learning
              many skills and worked with relevant teams to polish them. I am
              open to any opportunity to develop my skills and also myself.
            </p>
          </div>
        </div>

        <div className="flex-col w-3/4 sm:w-1/2" ref={rightSlide}>
          <h1 className="flex justify-center text-3xl">Contact Me</h1>
          <form
            ref={form}
            onSubmit={sendEmail}
            className="flex flex-col justify-center gap-2 pt-6 sm:p-6"
          >
            <input
              type="email"
              name="user_email"
              placeholder="Email"
              className="p-2 border-2 border-gray-800 rounded-lg"
              required
            />
            <input
              type="text"
              name="user_name"
              placeholder="Name"
              className="p-2 border-2 border-gray-800 rounded-lg"
              required
            />
            <input
              type="text"
              name="Contact"
              placeholder="Mobile Number(optional)"
              className="p-2 border-2 border-gray-800 rounded-lg"
            />
            <input
              type="text"
              name="message"
              placeholder="Content"
              className="pt-2 pb-24 pl-2 border-2 border-gray-800 rounded-lg"
              required
            />
            <button
              type="Submit"
              className="px-4 py-2 text-center text-white transition ease-in-out bg-black rounded-lg place-self-start w-fit h-fit hover:bg-slate-900 hover:shadow-md hover:shadow-black"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
      {sentAlert ? (
        <div
          class="bg-green-100 border-t border-b border-green-500 text-green-700 px-4 py-3 relative my-1 w-full "
          role="alert"
        >
          <p class="font-bold">Message Sent</p>
          <p class="text-sm">
            Thank you for Contacting Me. I will get in touch with you as soon as
            possible
          </p>
          <a onClick={() => setSentAlert(false)}>
            <RxCross1 className="absolute top-2 right-4 hover:cursor-pointer" />
          </a>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default Info;
