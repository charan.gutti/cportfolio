import React from "react";
import { Link } from "react-scroll";

const Navbar = () => {
  return (
    <div className="sticky top-0 z-10 flex w-full bg-white rounded-sm ">
      <div className="flex flex-row items-center justify-between w-full py-3 text-lg bg-white border-b-2 ">
        <div className="px-5 text-2xl ">C</div>
        <button
          data-collapse-toggle="navbar-multi-level"
          type="button"
          class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg xl:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
          aria-controls="navbar-multi-level"
          aria-expanded="false"
        >
          <svg
            class="w-5 h-5"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 17 14"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M1 1h15M1 7h15M1 13h15"
            />
          </svg>
        </button>

        <div className="hidden xl:flex xl:gap-20" id="navbar-multi-level">
          <Link to="home" offset={-100} smooth={true} spy={true}>
            <div className="hover:bg-gray-900 hover:text-white hover:rounded-lg rounded-lg hover:cursor-pointer hover:py-2.5 px-6 hover:border-black py-2.5 transition ease-in-out delay-100 hover:scale-105">
              Home
            </div>
          </Link>

          <Link to="e&e" smooth={true} spy={true} offset={-150}>
            <div className="hover:bg-gray-900 hover:text-white hover:rounded-lg rounded-lg hover:cursor-pointer hover:py-2.5 px-6 hover:border-black py-2.5 transition ease-in-out delay-100 hover:scale-105">
              Experience & Education
            </div>
          </Link>

          <Link to="projects" offset={-150} smooth={true} spy={true}>
            <div className="hover:bg-gray-900 hover:text-white hover:rounded-lg rounded-lg hover:cursor-pointer hover:py-2.5 px-6 hover:border-black py-2.5 transition ease-in-out delay-100 hover:scale-105">
              Projects
            </div>
          </Link>

          <Link to="info" smooth={true} spy={true} offset={-55}>
            <div className="hover:bg-gray-900 hover:text-white hover:rounded-lg rounded-lg hover:cursor-pointer hover:py-2.5 px-6 hover:border-black py-2.5 transition ease-in-out delay-100 hover:scale-105">
              Contact Me
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
