import gsap from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import React, { useEffect, useState } from "react";
import img1 from "../Images/1.png";
import img2 from "../Images/2.png";
import img3 from "../Images/3.png";
import img4 from "../Images/4.png";

gsap.registerPlugin(ScrollTrigger);

const Projects = () => {
  const data = [
    {
      name: "SIS App using React Native & UI Design with Figma(Mobile Application)",
      img: "Image1",
      no: 1,
      show: true,
      img: img1,
      clickAction:
        "https://www.figma.com/proto/qBeStz1JDCtVXDNFxIUyhv/Student-Portal?page-id=0%3A1&node-id=2-62&starting-point-node-id=2%3A62&mode=design&t=7DXX4aHL9Fz2qeOt-1",
    },
    {
      name: "UI Design for the Portfolio Website",
      img: "Image1",
      no: 2,
      show: false,
      img: img2,
      clickAction:
        "https://www.figma.com/file/DkQf9xpxIm1mp98p3cwnX6/Portfolio?type=design&node-id=0%3A1&mode=design&t=DwMaz47QnfidC6sV-1",
    },
    {
      name: "AI Finance Management app using React in front-end And python flask for back-end(API) operations",
      img: "Image1",
      no: 3,
      show: false,
      img: img3,
      clickAction: "",
    },
    {
      name: "Simple Registration & Login page with OTP verification using php in the backend",
      img: "Image1",
      no: 4,
      show: false,
      img: img4,
      clickAction: "",
    },
  ];
  const animations = () => {
    const ani = gsap.context(() => {
      gsap.fromTo(
        "#project",
        { duration: 2, opacity: 0 },
        {
          duration: 2,
          opacity: 1,
          ease: "power1",
          scrollTrigger: {
            trigger: "#project",
            start: "top 70%",
            end: "top 40%",
            scrub: true,
            markers: false,
          },
        }
      );
      gsap.fromTo(
        "#main",
        { duration: 2, y: 100 },
        {
          duration: 2,
          y: 0,
          ease: "power1",
          scrollTrigger: {
            trigger: "#main",
            start: "top 70%",
            end: "top 40%",
            scrub: true,
            markers: false,
          },
        }
      );
    });
    return () => ani.revert();
  };
  useEffect(() => {
    animations();
  });
  const [show, setShow] = useState(1);
  return (
    <div id="main">
      <section id="projects">
        <div className="h-auto text-white bg-black rounded-md ">
          <h1 className="flex justify-center py-8 pl-3 text-4xl" id="project">
            Projects
          </h1>
          {/* <div className="py-10 " id="project">
            <div className="flex justify-center scale-90 columns-2 gap-11 ">
              <svg
                width="600"
                height="350"
                className=" fill-white rounded-xl hover:scale-105"
              >
                <rect width="600" height="350" />
              </svg>
              <svg
                width="600"
                height="350"
                className=" fill-white rounded-xl hover:scale-105"
              >
                <rect width="600" height="350" />
              </svg>
            </div>
            <div className="flex justify-center gap-2 py-6 ">
              <div
                className="bg-white rounded-full hover:outline-dashed"
                style={{ width: "25px", height: "25px" }}
              ></div>
              <div
                className="bg-white rounded-full hover:outline-dashed"
                style={{ width: "25px", height: "25px" }}
              ></div>
              <div
                className="bg-white rounded-full hover:outline-dashed"
                style={{ width: "25px", height: "25px" }}
              ></div>
              <div
                className="bg-white rounded-full hover:outline-dashed"
                style={{ width: "25px", height: "25px" }}
              ></div>
            </div>
          </div> */}
          <div className="py-10 " id="project">
            {data.map((data) => {
              if (show == data.no) {
                return (
                  <div className="flex justify-center scale-100 columns-2 gap-11 ">
                    <div>
                      <a href={data.clickAction}>
                        <img
                          width="900"
                          height="450"
                          src={data.img}
                          className="transition-all cursor-pointer fill-white rounded-xl hover:scale-105"
                        />
                      </a>
                      {/* <svg
                        width="900"
                        height="450"
                        className="transition-all cursor-pointer fill-white rounded-xl hover:scale-105"
                      >
                        
                        <rect width="900" height="450" />
                      </svg> */}
                      <div className="w-full p-4 text-center pt-11">
                        {data.name}
                      </div>
                    </div>
                  </div>
                );
              }
            })}
            <div className="flex justify-center gap-2 py-6 ">
              {data.map((data) => (
                <div
                  className="text-center text-black transition-all bg-white rounded-full cursor-pointer hover:scale-125"
                  style={{ width: "25px", height: "25px" }}
                  onClick={() => {
                    setShow(data.no);
                  }}
                >
                  {data.no}
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Projects;
