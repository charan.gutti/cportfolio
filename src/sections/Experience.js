import React from "react";
import ExperienceData from "../Data/ExperienceData";
import svg2 from "../Images/svg2.png";
import gsap from "gsap";
import { useEffect } from "react";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const Experience = () => {
  const animations = () => {
    const ani = gsap.context(() => {
      gsap.fromTo(
        "#experience",
        { duration: 2, opacity: 0 },
        {
          duration: 2,
          opacity: 1,
          ease: "power1",
          stagger: 0.5,
          scrollTrigger: {
            trigger: "#experience",
            start: "top 70%",
            end: "top 10%",
            scrub: true,
            markers: false,
          },
        }
      );
    });
    return () => ani.revert();
  };

  useEffect(() => {
    animations();
  });
  return (
    <div className="px-4 pt-10 pb-32 text-black bg-transparent h-fit lg:px-14">
      <h1 className="flex justify-center py-10 pl-8 text-4xl" id="experience">
        Experience
      </h1>

      <div class="flex flex-col lg:gap-14 gap-6 lg:p-6 justify-center">
        {ExperienceData.map((data) => {
          return (
            //   <div className='flex justify-center' id="experience">
            //       <div className='pr-5 '>
            //       <div className=' pl-2.5'>
            //     <div className='w-1 xl:h-[15vw] lg:h-[20vw] sm:h-[30vw] h-[40vw]  bg-black content-none absolute'></div>
            //     </div>
            //           <div className='z-10 bg-black rounded-full shadow-lg hover:outline-dashed shadow-black' style={{ width: '25px', height: '25px' }}>
            //           </div>
            //       </div>
            //       <div className='p-3 bg-transparent border-2 border-black rounded-lg hover:scale-105 w-fit h-fit'>
            //       <div className='flex flex-row justify-start pb-3'>
            //               <img src={svg2} style={{ width: '32px' }} />
            //               <p className='pt-1 pl-2'>{data.year}</p>
            //           </div>
            //           <div className='overflow-hidden xl:w-[25vw] w-[21rem]'>
            //               <p>{data.work}</p>
            //           </div>
            //       </div>
            //   </div>
            <div className="flex justify-center" id="experience">
              <div className="pr-5 ">
                <div className=" pl-2.5">
                  <div className="w-[4px] -z-10 xl:h-[20vw] lg:h-[30vw] sm:h-[40vw] h-[80vw] bg-black content-none absolute"></div>
                </div>
                <div
                  className="z-10 transition ease-in-out bg-black rounded-full shadow-lg hover:outline-dashed shadow-black"
                  style={{ width: "25px", height: "25px" }}
                ></div>
              </div>
              <div className="p-3 overflow-hidden transition duration-75 ease-in-out bg-transparent border-2 border-black rounded-lg shadow-md hover:scale-105 w-fit h-fit shadow-black">
                <div className="flex flex-row justify-start pb-3">
                  <img src={svg2} style={{ width: "32px" }} />
                  <p className="pt-1 pl-2">{data.year}</p>
                </div>
                <div className="overflow-hidden xl:w-[25vw] sm:w-[21rem] w-[15rem]">
                  <p className="text-lg ">{data.work}</p>
                  <p className="text-sm italic opacity-75 text-start ">
                    {data.employee}
                  </p>
                  <p className="text-xs opacity-95">{data.content}</p>
                </div>
              </div>
            </div>
          );
        })}
        {/* <div className='flex flex-col justify-center p-6'>
              <div className='pb-6 '>
                  <div className='bg-black rounded-full hover:outline-dashed' style={{ width: '25px', height: '25px' }}>
                  </div>
              </div>
              <div className='p-3 bg-transparent border-2 border-black rounded-lg hover:scale-105 w-fit'>
                  <div className='flex flex-row justify-start pb-3'>
                      <img src={svg2} style={{ width: '32px' }} />
                      <p className='pt-1 pl-2'>2020-2021</p>
                  </div>
                  <p className=''>dsakjhdas accordiondsaojdas dsaghdasgd a dsaghdkasg sahdaskgdlsadgas</p>
              </div>
          </div> */}
      </div>
    </div>
  );
};

export default Experience;
