import "animate.css";
import gsap from "gsap";
import React, { useEffect, useRef } from "react";
import { AiFillLinkedin } from "react-icons/ai";
import { BsArrowUpRight } from "react-icons/bs";
import { FaGithub } from "react-icons/fa";
import { Link } from "react-scroll";
import Img from "../Images/Profile.jpg";

const LandingPage = () => {
  const IMG = useRef();

  const animations = () => {
    const ani = gsap.context(() => {
      gsap.fromTo(
        IMG.current,
        { duration: 1, x: 200, opacity: 0, delay: 0.5 },
        {
          duration: 1,
          x: 0,
          opacity: 1,
          delay: 0.5,
          ease: "power2",
          scrollTrigger: { trigger: IMG.current },
        }
      );
    });
    return () => ani.revert();
  };

  useEffect(() => {
    animations();
  });
  return (
    <section id="home">
      <div className="flex flex-col-reverse justify-center px-8 py-12 border-b-2 sm:py-18 lg:flex-row 2xl:justify-between lg:py-16 xl:py-14 md:py-14">
        <div className=" flex 2xl:flex-col flex-row justify-center 2xl:h-[40vw] 2xl:w-1/2 ">
          <div className="flex flex-col space-y-4 lg:space-y-0">
            <div className="self-center md:self-start md:p-6 ">
              <p className="pb-3 text-3xl drop-shadow-md animate__animated animate__fadeIn animate__delay-1s">
                Hey!! Im a FullStack Developer
              </p>
            </div>
            <div className="self-center md:self-start md:pl-6 md:py-6 animate__animated animate__fadeInUp animate__slow ">
              <span className="w-full mb-3 font-serif text-6xl md:text-7xl 2xl:text-8xl">
                Charan Gutti
              </span>
            </div>

            <div className="flex justify-center pt-4 md:justify-start">
              <div className="flex flex-row h-fit w-fit md:p-8 sm:gap-28 gap-14 animate__animated animate__fadeIn animate__delay-1s">
                <Link to="skillset" smooth={true} spy={true}>
                  <div className="flex justify-center px-4 py-2 text-lg text-white transition duration-75 ease-in-out rounded-lg hover:scale-105 md:text-2xl md:px-6 md:py-3 bg-slate-950 hover:bg-slate-900 hover:cursor-pointer w-fit h-fit hover:shadow-black hover:shadow-md">
                    Skillset
                  </div>
                </Link>
                <a
                  href="https://www.canva.com/design/DAF2sGHOJdY/_2RSBp8oD9iyVeA-6srjTQ/edit?utm_content=DAF2sGHOJdY&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton"
                  className="self-center text-xl text-black underline md:scale-125 underline-offset-4 drop-shadow-md hover:cursor-pointer"
                >
                  Download Resume
                </a>
              </div>
            </div>
            <div className="flex flex-col items-center space-y-4 sm:space-y-0 sm:space-x-8 sm:justify-start sm:flex-row lg:pl-8 sm:pl-0">
              <a
                href="https://github.com/g-charan"
                className="relative flex items-center w-3/4 gap-4 px-6 py-3 border-2 rounded-md 2xl:w-2/6 border-slate-200 hover:border-slate-900"
              >
                <a className="w-fit ">Visit Github</a>
                <FaGithub size={"4vh"} className=" sm:scale-100 sm:mx-0" />
                <BsArrowUpRight
                  size={20}
                  className="absolute top-0.5 right-0.5 opacity-75"
                />
              </a>
              <a
                href="https://www.linkedin.com/in/charan-gutti-b4b264206/"
                className="relative flex items-center w-3/4 gap-4 px-6 py-3 m-0 border-2 rounded-md 2xl:w-2/6 border-slate-200 hover:border-slate-900"
              >
                <a className="w-fit ">Visit LinkedIn</a>
                <AiFillLinkedin
                  size={"4vh"}
                  className=" sm:scale-100 sm:mx-0"
                />
                <BsArrowUpRight
                  size={20}
                  className="absolute top-0.5 right-0.5 opacity-75"
                />
              </a>
            </div>
          </div>
        </div>
        <div className="flex justify-center p-6 2xl:w-2/4 2xl:static">
          <img
            src={Img}
            ref={IMG}
            className="hidden 2xl:border-4 2xl:flex 2xl:border-black 2xl:hover:outline-dashed 2xl:w-fit 2xl:h-[40vw] 2xl:float-right shadow-2xl shadow-black rounded-md"
          />
          <img
            src={Img}
            className="object-cover justify-center flex float-right my-6 border-4 border-black rounded-full shadow-2xl h-[16rem] w-[16rem] 2xl:hidden hover:outline-dashed shadow-black"
          />
        </div>
      </div>
    </section>
  );
};

export default LandingPage;
