const ExperienceData = [
  {
    year: "2021",
    work: "Social Media Management",
    employee: "Personal",
    content:
      "I worked on my own personal channels in social media pages like YouTube, Instagram and Discord. I was confused and didn't have any strategy at first but I was able to quickly adapt to the environment which helped me development strategies where I also hit a view count of over 900,000 views in YouTube.",
  },
  {
    year: "August 2022",
    work: "Social Media Marketer",
    employee: "SKG(Shadow Knight Gaming)",
    content:
      "I worked as a social media marketer where my role included the management of different social media pages like Facebook, Instagram  and YouTube. My role also included creating content on a daily basis by researching through the internet and also engaging our audience. I was able to grow their reach by 3x then before.",
  },
  {
    year: "Sept 2022 - Dec 2022",
    work: "SM Management/marketing",
    employee: "Harshad Bhagwat",
    content:
      "This internship included me taking care all the funnels in the Digital marketing including posting on social media platforms for engagement and awareness, converting audience into leads and also managing lead groups.(Theme : Career Coach)",
  },
  {
    year: "June 2023 - Present",
    work: "Worked on a SAAS Application",
    content:
      "The application solves the problem of providing student portal with users and admin panel segregations and can be used by any organization. I developed simple crud operations and page frontend. I used MERN stack development (Note: It's still in development)",
    employee: "Personal",
  },
  {
    year: "Aug 2023 - Present",
    work: " Made my React Portfolio with advanced animations",
    employee: "Personal",
    content:
      "I wanted to showcase my web development skills and I thought that Portfolio website would be the best solution for that.",
  },
];

export default ExperienceData;
