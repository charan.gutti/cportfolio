import Html from "../Images/Html.png";
import Css from "../Images/Css.png";
import C from "../Images/C.png";
import React from "../Images/React.png";
import Javascript from "../Images/Javascript.png";

const BaseComponents = [
  {
    img: Html,
  },
  {
    img: Css,
  },
  {
    img: Javascript,
  },
  {
    img: React,
  },
  {
    img: C,
  },
];

export default BaseComponents;
