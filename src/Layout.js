import React from "react";
import LandingPage from "./sections/LandingPage";
import Navbar from "./Components/Navbar";
import Skillset from "./sections/Skillset";
import Education from "./sections/Education";
import Experience from "./sections/Experience";
import Projects from "./sections/Projects";
import Info from "./sections/Info";
import Footer from "./Components/Footer";

const Layout = () => {
  return (
    <div className="flex justify-center ">
      <div className="flex flex-col justify-center w-screen border-b-2 xl:w-3/4 ">
        <Navbar />

        <div className="overflow-hidden ">
          <LandingPage />

          <Skillset />
          <Education />
          <Experience />
          <Projects />
          <Info />
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Layout;
