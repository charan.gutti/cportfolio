import React, { useRef } from "react";
import EducationData from "../Data/EducationData";
import svg1 from "../Images/svg1.png";
import gsap from "gsap";
import { useEffect } from "react";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const Education = () => {
  const main = useRef();
  const animations = () => {
    const ani = gsap.context(() => {
      gsap.fromTo(
        "#education",
        { duration: 2, opacity: 0 },
        {
          duration: 2,
          opacity: 1,
          ease: "power1",
          stagger: 0.5,
          scrollTrigger: {
            trigger: "#education",
            start: "top 70%",
            end: "top 10%",
            scrub: true,
            markers: false,
          },
        }
      );
      gsap.fromTo(
        main.current,
        { duration: 2, y: 100 },
        {
          duration: 2,
          y: 0,
          ease: "power1",
          scrollTrigger: {
            trigger: main.current,
            start: "top 70%",
            end: "top 10%",
            scrub: true,
            markers: false,
          },
        }
      );
    });
    return () => ani.revert();
  };

  useEffect(() => {
    animations();
  });
  return (
    <div
      className="px-4 pt-10 pb-32 text-white bg-black rounded-md h-fit lg:px-14"
      id="e&e"
      ref={main}
    >
      <h1 className="flex justify-center py-10 pl-8 text-4xl " id="education">
        Education
      </h1>

      <div className="flex flex-col justify-center gap-6 lg:gap-14 lg:p-6">
        {EducationData.map((data) => {
          return (
            <div className="flex justify-center" id="education">
              <div className="pr-5 ">
                <div className=" pl-2.5">
                  <div className="w-[4px] -z-10 xl:h-[15vw] lg:h-[20vw] sm:h-[30vw] h-[50vw] bg-white content-none absolute"></div>
                </div>
                <div
                  className="z-10 transition ease-in-out bg-white rounded-full shadow-lg hover:outline-dashed shadow-black"
                  style={{ width: "25px", height: "25px" }}
                ></div>
              </div>
              <div className="p-3 overflow-hidden transition duration-75 ease-in-out bg-transparent border-2 border-white rounded-lg shadow-md hover:scale-105 w-fit h-fit shadow-white">
                <div className="flex flex-row justify-start pb-3">
                  <img src={svg1} style={{ width: "32px" }} />
                  <p className="pt-1 pl-2">{data.year}</p>
                </div>
                <div className="overflow-hidden xl:w-[25vw] sm:w-[21rem] w-[15rem]  ">
                  <p className="">{data.school}</p>
                  <p>{` Score : ${data.CGPA}`}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Education;
