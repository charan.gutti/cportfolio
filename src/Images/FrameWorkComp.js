import Tailwind from "../Images/tailwind.png";
import Gsap from "../Images/gsap.svg";
import Redux from "../Images/redux.svg";
import React from "../Images/Bootstrap.png";
import Javascript from "../Images/Nextjs2.png";

const FrameWorkComp = [
  {
    img: Tailwind,
  },
  {
    img: Gsap,
  },
  {
    img: Javascript,
  },
  {
    img: React,
  },
  {
    img: Redux,
  },
];

export default FrameWorkComp;
