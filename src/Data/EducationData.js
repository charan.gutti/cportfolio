const EducationData = [
  {
    year: "2015 - 2020",
    school: "Completed my class 10th at Epistemo Global Schoool ",
    CGPA: "90% (CBSE Board)",
  },
  {
    year: "2020 - 2022",
    school: "Completed my Class 12th at Vikas The Concept School ",
    CGPA: "80% (CBSE Board) ",
  },
  {
    year: "2022 - 2026",
    school: "Currently Studying B.tech(CSE) at ICFAI Tech ",
    CGPA: "8.2/10 (First Year Results)",
  },
];

export default EducationData;
