import React, { useEffect, useRef } from "react";
import BaseComponents from "../Images/BaseComponents";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import FrameWorkComp from "../Images/FrameWorkComp";
gsap.registerPlugin(ScrollTrigger);

const Skillset = () => {
  const animations = () => {
    const ani = gsap.context(() => {
      gsap.fromTo(
        "#skills",
        { duration: 3, opacity: 0 },
        {
          duration: 3,
          opacity: 1,
          ease: "power1",
          stagger: 0.1,
          scrollTrigger: {
            trigger: "#skills",
            start: "top 90%",
            end: "top 30%",
            scrub: true,
            markers: false,
          },
        }
      );
    });
    return () => ani.revert();
  };

  useEffect(() => {
    animations();
  });
  return (
    <section id="skillset">
      <div className=" py-14">
        <div className="space-y-8 ">
          <h1 className="flex justify-center p-6 text-4xl ">Base</h1>
          <div className="flex flex-row justify-center gap-4 ">
            {BaseComponents.map((base) => {
              return (
                <div
                  id="skills"
                  className=" transition duration-75 ease-in-out bg-black rounded-full relative flex items-center justify-center hover:scale-110  overflow-hidden w-[3.5rem] h-[3.5rem] sm:w-[4.5rem] sm:h-[4.5rem] md:w-[80px] md:h-[80px]"
                >
                  <img
                    src={base.img}
                    className="absolute justify-self-center sm:h-[3rem] sm:w-auto md:h-[50px] h-[2.5rem] "
                  />
                </div>
              );
            })}
          </div>
          <h1 className="flex justify-center p-6 text-4xl">FrameWorks</h1>
          <div className="flex flex-row justify-center gap-4 pb-6 ">
            {FrameWorkComp.map((base) => {
              return (
                <div
                  id="skills"
                  className="transition duration-75 ease-in-out bg-black rounded-full relative flex items-center justify-center hover:scale-110 w-[3.5rem] h-[3.5rem] sm:w-[4.5rem] sm:h-[4.5rem] md:w-[80px] md:h-[80px]"
                >
                  <img
                    src={base.img}
                    className="absolute justify-self-center sm:h-[3rem] sm:w-auto md:h-[50px] h-[2.5rem]"
                  />
                </div>
              );
            })}
          </div>

          <div
            class="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3"
            role="alert"
          >
            <p class="font-bold">Note :</p>
            <p class="text-sm">
              For complete details on the programs I learnt. Please refer the
              resume.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Skillset;
